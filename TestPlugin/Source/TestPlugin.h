#ifndef TEST_PLUGIN_SOURCE_TEST_PLUGIN_H
#define TEST_PLUGIN_SOURCE_TEST_PLUGIN_H 1

class TestPluginEditor : public juce::AudioProcessorEditor
{
public:
    enum
    {
        paramControlHeight = 40,
        paramLabelWidth    = 80,
        paramSliderWidth   = 300
    };

    typedef juce::AudioProcessorValueTreeState::SliderAttachment SliderAttachment;

    TestPluginEditor (juce::AudioProcessor& parent, juce::AudioProcessorValueTreeState& vts)
        : AudioProcessorEditor (parent),
          valueTreeState (vts)
    {
        gainLabel.setText ("Gain", juce::dontSendNotification);
        addAndMakeVisible (gainLabel);

        addAndMakeVisible (gainSlider);
        gainAttachment = std::make_unique<SliderAttachment> (valueTreeState, "gain", gainSlider);

        setSize (paramSliderWidth + paramLabelWidth, juce::jmax (100, paramControlHeight * 2));
    }

    void resized() override
    {
        auto rect = getLocalBounds();

        auto gainRect = rect.removeFromTop (paramControlHeight);
        gainLabel.setBounds (gainRect.removeFromLeft (paramLabelWidth));
        gainSlider.setBounds (gainRect);
    }

    void paint (juce::Graphics& g) override
    {
        g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));
    }

private:
    AudioProcessorValueTreeState& valueTreeState;

    Label gainLabel;
    Slider gainSlider;
    std::unique_ptr<SliderAttachment> gainAttachment;
};

//==============================================================================

class TestPluginProcessor  : public juce::AudioProcessor
{
public:
    TestPluginProcessor() : parameters (*this, nullptr)
    {
        parameters.createAndAddParameter ("gain", "Gain", "Gain", NormalisableRange<float> (0.0f, 1.0f), 0.5f, nullptr, nullptr);
        gainParameter = parameters.getRawParameterValue ("gain");
        parameters.state = ValueTree ("TestPlugin");
    }

    void prepareToPlay (double, int) override
    {
        previousGain = *gainParameter;
    }

    void releaseResources() override {}

    void processBlock (juce::AudioSampleBuffer& buffer, juce::MidiBuffer&) override
    {
        auto currentGain = *gainParameter;

        if (currentGain == previousGain)
        {
            buffer.applyGain (currentGain);
        }
        else
        {
            buffer.applyGainRamp (0, buffer.getNumSamples(), previousGain, currentGain);
            previousGain = currentGain;
        }
    }

    juce::AudioProcessorEditor* createEditor() override          { return new TestPluginEditor (*this, parameters); }
    bool hasEditor() const override                              { return true; }

    const juce::String getName() const override                  { return "Test plugin"; }
    bool acceptsMidi() const override                            { return false; }
    bool producesMidi() const override                           { return false; }
    double getTailLengthSeconds() const override                 { return 0; }

    int getNumPrograms() override                                { return 1; }
    int getCurrentProgram() override                             { return 0; }
    void setCurrentProgram (int) override                        {}
    const juce::String getProgramName (int) override             { return {}; }
    void changeProgramName (int, const juce::String&) override   {}

    //==============================================================================
    
    void getStateInformation (juce::MemoryBlock& destData) override
    {
        auto state = parameters.state.createCopy();
        std::unique_ptr<juce::XmlElement> xml (state.createXml());
        copyXmlToBinary (*xml, destData);
    }

    void setStateInformation (const void* data, int sizeInBytes) override
    {
        std::unique_ptr<juce::XmlElement> xmlState (getXmlFromBinary (data, sizeInBytes));

        if (xmlState.get() != nullptr)
            if (xmlState->hasTagName (parameters.state.getType()))
                parameters.state = juce::ValueTree::fromXml (*xmlState);
    }

private:
    AudioProcessorValueTreeState parameters;
    
    float previousGain;
    float* gainParameter  = nullptr;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (TestPluginProcessor)
};

#endif // TEST_PLUGIN_SOURCE_TEST_PLUGIN_H
